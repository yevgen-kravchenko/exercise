name := "userserver"
version := "1.0"
libraryDependencies ++= Seq(
//  "com.twitter" %% "finagle-http" % "19.8.0",
  "com.github.finagle" %% "finch-core" % "0.22.0",
  "com.github.finagle" %% "finch-circe" % "0.22.0",
  "io.circe" %% "circe-generic" % "0.9.0"
)