import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.finagle.{Http, Service}
import com.twitter.util.Await
import io.finch._
import io.finch.syntax._
import io.circe.generic.auto._
import io.finch.circe._


object Server extends App {

  var getUserProfile: Endpoint[UserProfile] = get("user" :: path[Int] :: "profile") {
    id: Int =>
      val userProfile: Option[UserProfile] = FindUserProfile(id)
      userProfile match {
        case Some(value) => Ok(value)
        case None => Output.empty(Status.NotFound)
      }
  }

  var getUserDetails: Endpoint[UserDetails] = get("user" :: path[Int] :: "details") {
    id: Int =>
      val userDetails: Option[UserDetails] = FindUserDetails(id)
      userDetails match {
        case Some(u) => Ok(u)
        case None => Output.empty(Status.NotFound)
      }
  }

  val api: Service[Request, Response] = (getUserProfile :+: getUserDetails).toServiceAs[Application.Json]

  Await.ready(Http.server.serve(":8080", api))
}