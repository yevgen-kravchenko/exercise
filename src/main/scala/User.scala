case class UserProfile(id: Int, name: String)

case class UserDetails(id: Int, name: String, age: Int, country: String)


object FindUserDetails {
  val users = List(
    UserDetails(1, "Eugene", 35, "Ukraine"),
    UserDetails(2, "Helen", 34, "Ukraine"),
    UserDetails(3, "Andrii", 30, "Poland"),
    UserDetails(4, "Yurii", 32, "Germany")
  )

  def apply(id: Int): Option[UserDetails] = users.find(u => u.id == id)
}

object FindUserProfile {

  def apply(id: Int): Option[UserProfile] = FindUserDetails(id).map(u => UserProfile(u.id, u.name))
}